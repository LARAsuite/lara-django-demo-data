# LARA-django Demo Data

Demo Data Provider for LARA-django

## Features

## Installation

    pip install lara_django_demo_data --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    lara_django_demo_data --help 

## Development

    git clone gitlab.com/LARAsuite/lara-django-demo-data

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://LARAsuite.gitlab.io/lara-django-demo-data](https://LARAsuite.gitlab.io/lara-django-demo-data) or [lara-django-demo-data.gitlab.io](lara_django_demo_data.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



